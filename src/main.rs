use orbtk::prelude::*;
use std::process::Command;
use orbtk::prelude::utils::Point;

fn main() {
    Application::new()
        .window(|ctx| {
            Window::new()
                .title("firefox launcher")
                .position((100.0, 100.0))
                .size(420.0, 420.0)
                .child(MainView::new().build(ctx))
                .build(ctx)
            }
    ).run();
}

widget!(
    MainView {}
);

impl Template for MainView {
    fn template(self, id: Entity, ctx: &mut BuildContext) -> Self { 
        self.name("MainView")
            .child(
                Grid::new()
                    .rows(Rows::new().add(30.0).add(72.0).add("*"))
                    .child(
                        Grid::new()
                            .element("textyee")
                            .attach(Grid::row(0))
                            .child(
                                TextBlock::new()
                                    .text("Application Launcher")
                                .build(ctx)
                            )
                        .build(ctx)
                    )
                    .child(
                        Grid::new()
                            .element("lff")
                            .attach(Grid::row(1))
                            .child(
                                Button::new()
                                    .background("#4287f5")
                                    .on_click(
                                        move |_,_| {
                                            println!("launching firefox");
                                            Command::new("open").arg("-afirefox").output().unwrap();
                                           true
                                        }
                                    )
                                    .text("Launch Firefox")
                                .build(ctx)
                            )
                        .build(ctx)
                    )
                    .child(
                        Grid::new()
                            .element("ls")
                            .attach(Grid::row(2))
                            .child(
                                Button::new()
                                .background("#f54242")
                                .on_click(
                                    move |_,_| {
                                        println!("Launching spotify");
                                        Command::new("open").arg("-amusic").output().unwrap();
                                        true
                                    }
                                )
                                .text("Launch Itunes")
                                .build(ctx)
                            )
                        .build(ctx)
                    )
                    
                    .build(ctx)
            )
    }
}

